---
layout: page
title:
---

Tukaj je baza znanja za maximo developerje. Vsebuje rešitve za pogoste probleme, ki jih maximo developerji srečujejo. Ta
stran se spotoma dopolnjuje s strani TROIA developerjev.

Link do git repositorja z navodili za inštalacijo in vzpostavitev okolja:
[maximo-dev-handbook](http://git.troia.si/bkamn/maximo-dev-handbook)


### Pomoč pri pisanju člankov
- [Markdown basics](https://www.markdownguide.org/basic-syntax/) (preprosta sintaksa, ki se uporablja pri pisanju)
- [Jekyll](https://jekyllrb.com/docs/) (framework, ki stoji za to spletno stran)]

## Teme
{% for topic in site.topics %}
<p class="post-title" style="font-size:24px"><a href="{{site.baseurl}}{{topic.url}}">{{topic.title}}</a></p>
{% endfor %}